<?php

namespace App;

use RefineriaWeb\RWRealEstate\Models\PropertyFeature;

/**
 * Class PropertyFeatureModel
 *
 */
class PropertyFeatureModel extends PropertyFeature
{
    /**
     * Get Features Property
     */
    public function properties()
    {
        return $this->hasOne('App\PropertyModel', 'id', 'property_id');
    }

}
