<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PropertyModel;
use App\LocationModel;

class PropertyController extends Controller
{

	/**
     * Show Properties.
     *
     * @return View
     */
	public function index()
	{
		$properties = PropertyModel::all()->random(10);

		return view('vendor.rw-real-estate.home', ['properties' => $properties]);
	}

    /**
     * Show Properties filter view.
     *
     * @return View
     */
    public function filter()
    {
    	$properties = PropertyModel::join('properties_features', 'properties_features.property_id', '=', 'properties.id')
    	->orderBy('properties_features.price', 'desc')
    	->select('properties.*')->paginate(20);

    	$locations = LocationModel::orderBy('name')->get();

    	return view('vendor.rw-real-estate.properties', ['properties' => $properties, 'locations' => $locations]);
    }

    /**
     * Show property profile.
     *
     * @return View
     */
    public function show($id)
    {
    	$property = PropertyModel::where('id', $id)->first();

    	return view('vendor.rw-real-estate.propertyProfile', ['property' => $property]);
    }

    /**
     * Get filtered properties.
     *
     * @return View
     */
    public function getFilteredProperties(Request $request)
    {

    	dd($request->all());
    	$name = $request->get('name') ==  null ? '' : $request->get('name');

    	if($request->get('name') ==  null) {
    		$name = '';
    	} else {

    		$properties = PropertyModel::with('features', 'location', 'types')->where('name', 'LIKE', '%'.$name.'%')->paginate(20);

    		return response()->json([
			    'pagination' => $properties
			]);
    	}

		// Property type
    	$property_type = $request->get('property_type');

    	// Property location
    	$location = $request->get('location');

    	// Property features
		$price_from = $request->get('$price_from') ==  null ? 0 : $request->get('$price_from');

		$price_to = $request->get('$price_to') ==  null ? 999999 : $request->get('$price_to');

    	$rooms = $request->get('rooms');
    	$bathrooms = $request->get('bathrooms');
    	$private_pool = $request->get('private_pool');
    	$community_pool = $request->get('community_pool');
    	$reference = $request->get('reference');
    	$garden = $request->get('garden');
    	$garaje = $request->get('garaje');

    	$properties = PropertyModel::with(['features' => function($query) use ($price_from, $price_to ,$rooms, $bathrooms, $private_pool, $community_pool, $reference) {
    		$query->whereBetween('price', [$price_from, $price_to])
    		->where('bedrooms', '>=', $rooms)
    		->where('bathrooms', '>=', $bathrooms)
    		->where('private_pool', '=', $private_pool)
    		->where('community_pool', '=', $community_pool)
    		->where('garden', '=', $garden)
    		->where('garaje', '=', $garaje)
    		->where('reference', 'LIKE', '%'.$reference.'%');
    	}], ['location' => function($query) use ($location) {
    		$query->where('id', '=', $location);
    	}],
    	['types' => function($query) use ($property_type) {
    		$query->where('id', '=', $property_type);
    	}])
    	->where('name', 'LIKE', '%'.$name.'%')
    	->paginate(20);

    	return response()->json([
		    'pagination' => $properties
		]);
    }
}