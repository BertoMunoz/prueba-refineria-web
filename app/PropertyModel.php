<?php

namespace App;

use RefineriaWeb\RWRealEstate\Models\Property;

/**
 * Class PropertyModel
 *
 */
class PropertyModel extends Property
{
    /**
     * Get Properties agent
     */
    public function agent()
    {
        return $this->belongsTo('App\AgentModel');
    }

    /**
     * Get property location
     */
    public function location()
    {
        return $this->belongsTo('App\LocationModel');
    }

    /**
     * Get property types
     */
    public function types()
    {
        return $this->hasOne('App\PropertyTypeModel', 'id', 'properties_type_id');
    }


    /**
     * Get property features
     */
    public function features()
    {
        return $this->belongsTo('App\PropertyFeatureModel', 'id', 'property_id');
    }
}
