<?php

namespace App;

use RefineriaWeb\RWRealEstate\Models\Location;

/**
 * Class Location
 * @package RefineriaWeb\RWRealEstate\Models
 */
class LocationModel extends Location
{
    /**
     * Get Agent properties
     */
    public function properties()
    {
        return $this->hasOne('App\PropertyModel', 'location_id');
    }
}
