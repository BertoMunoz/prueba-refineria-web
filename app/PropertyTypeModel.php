<?php

namespace App;

use RefineriaWeb\RWRealEstate\Models\PropertyType;

/**
 * Class PropertyType
 * @package RefineriaWeb\RWRealEstate\Models
 */
class PropertyTypeModel extends PropertyType
{
    /**
     * Get Agent properties
     */
    public function properties()
    {
        return $this->belongsTo('App\PropertyModel');
    }
}
