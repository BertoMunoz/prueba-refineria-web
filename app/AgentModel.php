<?php

namespace App;

use RefineriaWeb\RWRealEstate\Models\Agent;

/**
 * Class AgentModel
 *
 */
class AgentModel extends Agent
{

   /**
     * Get Agent properties
     */
    public function properties()
    {
        return $this->hasMany('App\PropertyModel', 'agent_id', 'id');
    }

}
