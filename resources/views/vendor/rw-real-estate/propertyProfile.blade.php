@extends('vendor.rw-real-estate.layouts.base')

@section('content')

<div class="title m-b-md">
    <h1 class="c-title c-title--h1">Property {!! $property->id !!}</h1>
</div>

<div class="c-grid">
    <aside>
        <section class="c-properties-wrapper">
            <div class="c-properties">
             <div class="c-properties-grid__item">
                <h3 class="c-properties-grid-item__title">Title {!! $property->name !!}</h3>
                <p>{{ $property->description }}</p>
                <ul class="c-properties-grid-item__list">
                    <li><b>Price:</b> {{ number_format($property->features->price, 2) }} €</li>
                    <li><b>Location:</b> {{ $property->location->name }}</li>
                    <li><b>Rooms:</b> {{ $property->features->bedrooms }}</li>
                    <li><b>Bathrooms:</b> {{ $property->features->bathrooms }}</li>
                    <li><b>Built Area:</b> {{ $property->features->built_area }} m2</li>
                </ul>
                <ul class="c-properties-grid-item__list">
                    <li><b>Type:</b> {{ $property->types->type }}</li>
                    <li><b>Agent:</b> {{ $property->agent->name }} {{ $property->agent->surname}}</li>
                </ul>
            </div>
        </div>
    </section>
</aside>
</div>

@endsection

@push('css')
<style>
    .c-grid {
        padding: 2rem;
        display: flex;
        justify-content: center;
    }
</style>
@endpush

@push('js')
<script>

</script>
@endpush