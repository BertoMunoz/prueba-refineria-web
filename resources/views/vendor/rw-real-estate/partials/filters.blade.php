<section class="c-filters">
    <h2 class="c-filters__title">Filters:</h2>
    <label for="name">
        Name: <input type="text" name="name" id="name" />
    </label>
    </br>
    <label for="price_from">
        Price from: <input type="number" name="price_from" id="price_from" />
    </label>
    </br>
    <label for="price_to">
        Price To: <input type="number" name="price_to" id="price_to" />
    </label>
    <br>
    <label for="rooms">
        Rooms:
        <select name="rooms" id="rooms">
            @for($i=1; $i<=10; $i++)
            <option value="{!! $i !!}">{!! $i !!}</option>
            @endfor
        </select>
    </label>
    <br>
    <label for="bathrooms">
        Bathrooms:
        <select name="bathrooms" id="bathrooms">
            @for($i=1; $i<=10; $i++)
            <option value="{!! $i !!}">{!! $i !!}</option>
            @endfor
        </select>
    </label>
    <br>
    <label for="property_type">
        Property Type:
        <select name="property_type" id="property_type">
            <option value="1">Semi Detached House</option>
            <option value="2">Apartment</option>
            <option value="3">Villa - Chalet</option>
            <option value="4">Cottage</option>
            <option value="5">Commercial</option>
            <option value="6">Plot</option>
        </select>
    </label>
    <br>
    <label for="location">
        Location:
        <select name="location" id="location">
            @foreach($locations as $location)
                <option value="{{ $location->id }}">{{ $location->name }}</option>
            @endforeach
        </select>
    </label>
    <br>
    <label for="garaje">
        Garaje:
        <input type="checkbox" name="garaje" id="garaje">
    </label>
    <br>
    <label for="garden">
        Garden:
        <input type="checkbox" name="garden" id="garden">
    </label>
    <br>
    <label for="private_pool">
        Private pool:
        <input type="checkbox" name="private_pool" id="private_pool">
    </label>
    <br>
    <label for="community_pool">
        Community pool:
        <input type="checkbox" name="community_pool" id="community_pool">
    </label>
    <br>
    <label for="reference">
        Reference:
        <input type="text" name="reference" id="reference">
    </label>
    <br>
    <input type="button" value="Filter" id="filter">
</section>

@push('css')
<style>
    .c-filters {
        text-align: left;
        padding: 15px;
    }
    .c-filters__title {
        text-align: left;
    }
</style>
@endpush

@push('js')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#filter").click(function(e){

        e.preventDefault();

        var name = $("#name").val();
        var price_from = $("#price_from").val();
        var price_to = $("#price_to").val();
        var rooms = $("#rooms").children("option:selected").val();
        var bathrooms = $("#bathrooms").children("option:selected").val();
        var property_type = $("#property_type").children("option:selected").val();
        var location = $("#location").children("option:selected").val();
        var garaje = $("#garaje").children("option:selected").val();
        var garden = $("#garden").children("option:selected").val();
        var private_pool = $("#property_type").children("option:selected").val();
        var community_pool = $("#property_type").children("option:selected").val();
        var reference = $("#reference").val();

        $.ajax({
            type:'POST',
            url: '{{ route('filter') }}',
            data:{
                name: name,
                price_from: price_from, 
                price_to: price_to, 
                rooms: rooms,
                bathrooms: bathrooms,
                property_type: property_type,
                location: location,
                garaje: garaje,
                garden: garden,
                private_pool: private_pool,
                community_pool: community_pool,
                reference: reference
            },
            success:function(response){
                console.log(response.pagination.data);

                var element = $('.c-properties-grid__wrapper')

                $('.c-properties-grid').empty();

                $.each(response.pagination.data, function(key, value) {

                    var content = '<div class="c-properties-grid__item"><h3 class="c-properties-grid-item__title">'+ value.name +'</h3><p>'+ value.description +'</p><ul class="c-properties-grid-item__list"><li>Price:'+ value.features.price +'€</li><li>Location:'+ value.location.name +'</li><li>Rooms:'+ value.features.bedrooms +'</li><li>Bathrooms:'+ value.features.bathrooms  +'</li><li>Built Area: '+ value.features.built_area +' m2</li></ul></div>';

                    element.append(content);
                });
            }
        });

    });
</script>
@endpush
