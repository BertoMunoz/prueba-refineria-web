<div class="c-properties-grid__item">
    <h3 class="c-properties-grid-item__title">{!! $property->name !!}</h3>
    <p>{{ $property->description }}</p>
    <ul class="c-properties-grid-item__list">
        <li>Price: {{ number_format($property->features->price, 2) }} €</li>
        <li>Location: {{ $property->location->name }}</li>
        <li>Rooms: {{ $property->features->bedrooms }}</li>
        <li>Bathrooms: {{ $property->features->bathrooms }}</li>
        <li>Built Area: {{ $property->features->built_area }} m2</li>
    </ul>
    <a href="{{ route('profile' , $property->id )}}">Details</a>
</div>
